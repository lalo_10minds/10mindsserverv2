
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

var commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    author:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

const platoSchema = new Schema({
  name:{
    type: String,
    required: true,
    unique: true
  },
  description:{
    type: String,
    requiered: true
  },
  category:{
    type: String,
    required: true
  },
  price:{
    type: Currency,
    require: true,
    min: 0
  },
  comments:[commentSchema]
},{
  timestamps: true
});

var Platos= mongoose.model('Plato',platoSchema);

module.exports = Platos;
